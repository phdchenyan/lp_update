from scipy.stats import binom
import pulp as pulp
from pulp import *
import copy
import numpy as np
import random
from tqdm import tqdm
import joblib
from joblib import Parallel, delayed
from candidate_selection_multi import *

# Fix N, generate one model; and simulate on this model nb_repeats times
def fairness_fix_N_one_model(T,N,P0,P1,P2,R,alpha,gamma0,gamma1,beta,max_Q,nb_repeats=10,true_value=True,correct_prior=True):
    if correct_prior:
        candidate = generate_candidate(N)
    else:
        candidate = generate_candidate(N,portion=0.5,prior0=[1,1],prior1=[2,2],true_prior0=[1,1],true_prior1=[3,1])
    perf_fair = []
    perf_unfair = []
    for _ in range(nb_repeats):
        res_fair = LP_update_recursive(candidate,T,N,P0,P1,P2,R,alpha,gamma0,gamma1,beta,max_Q)
        res_unfair = LP_update_recursive_unfair(candidate,T,N,P0,P1,P2,R,alpha,beta,max_Q)
        perf1 = perf_measure(res_fair,beta,true_value)
        perf2 = perf_measure(res_unfair,beta,true_value)
        perf_fair.append(perf1)
        perf_unfair.append(perf2)
    return np.mean(perf_fair),np.mean(perf_unfair)

# Fix N, simulate on nb_models of models
def fairness_fix_N_many_models(T,N,P0,P1,P2,R,alpha,gamma0,gamma1,beta,max_Q,nb_repeats=10,nb_models=160,true_value=True,correct_prior=True):
    perf_fair = []
    perf_unfair = []
    PERF = Parallel(n_jobs=-1)(delayed(fairness_fix_N_one_model)(T,N,P0,P1,P2,R,alpha,gamma0,gamma1,beta,max_Q,nb_repeats,true_value, \
                                                                 correct_prior) for _ in tqdm(range(nb_models)))
    for item in PERF:
        perf_fair.append(item[0])
        perf_unfair.append(item[1])
    fair_mean,fair_var = compute_mean_and_var(perf_fair)
    unfair_mean,unfair_var = compute_mean_and_var(perf_unfair)
    return [fair_mean,fair_var],[unfair_mean,unfair_var]

def heuristic_fix_N_one_model(T,N,P0,P1,P2,R,alpha,beta,max_Q,nb_repeats=10,true_value=True,correct_prior=True):
    if correct_prior:
        candidate = generate_candidate(N)
    else:
        candidate = generate_candidate(N,portion=0.5,prior0=[1,1],prior1=[2,2],true_prior0=[1,1],true_prior1=[3,1])
    perf_update = []
    perf_random = []
    perf_interview_all = []
    for _ in range(nb_repeats):
        res_update = LP_update_recursive_unfair(candidate,T,N,P0,P1,P2,R,alpha,beta,max_Q)
        res_random = random_selection_unfair(candidate,T,alpha)
        res_interview_all = interview_all_with_two_questions_heuristic(candidate)
        perf1 = perf_measure(res_update,beta,true_value)
        perf2 = perf_measure(res_random,beta,true_value)
        perf3 = perf_measure(res_interview_all,beta,true_value)
        perf_update.append(perf1)
        perf_random.append(perf2)
        perf_interview_all.append(perf3)
    return np.mean(perf_update),np.mean(perf_random),np.mean(perf_interview_all)

def heuristic_fix_N_many_models(T,N,P0,P1,P2,R,alpha,beta,max_Q,nb_repeats=10,nb_models=160,true_value=True,correct_prior=True):
    perf_update = []
    perf_random = []
    perf_interview_all = []
    PERF = Parallel(n_jobs=-1)(delayed(heuristic_fix_N_one_model)(T,N,P0,P1,P2,R,alpha,beta,max_Q,nb_repeats,true_value,correct_prior) \
                               for _ in tqdm(range(nb_models)))
    for item in PERF:
        perf_update.append(item[0])
        perf_random.append(item[1])
        perf_interview_all.append(item[2])
    update_mean,update_var = compute_mean_and_var(perf_update)
    random_mean,random_var = compute_mean_and_var(perf_random)
    interview_all_mean,interview_all_var = compute_mean_and_var(perf_interview_all)
    return [update_mean,update_var],[random_mean,random_var],[interview_all_mean,interview_all_var]

def fairness_alone_fix_N_one_model(T,N,P0,P1,P2,R,alpha,gamma0,gamma1,beta,max_Q,nb_repeats=10,true_value=True,correct_prior=True):
    if correct_prior:
        candidate = generate_candidate(N)
    else:
        candidate = generate_candidate(N,portion=0.5,prior0=[1,1],prior1=[2,2],true_prior0=[1,1],true_prior1=[3,1])
    perf_fair = []
    for _ in range(nb_repeats):
        res_fair = LP_update_recursive(candidate,T,N,P0,P1,P2,R,alpha,gamma0,gamma1,beta,max_Q)
        perf = perf_measure(res_fair,beta,true_value)
        perf_fair.append(perf)
    return np.mean(perf_fair)

def fairness_alone_fix_N_many_models(T,N,P0,P1,P2,R,alpha,gamma0,gamma1,beta,max_Q,nb_repeats=10,nb_models=160,true_value=True,correct_prior=True):
    perf_fair = Parallel(n_jobs=-1)(delayed(fairness_alone_fix_N_one_model)(T,N,P0,P1,P2,R,alpha,gamma0,gamma1,beta,max_Q,nb_repeats, \
                                                              true_value,correct_prior) for _ in tqdm(range(nb_models)))
    fair_mean,fair_var = compute_mean_and_var(perf_fair)
    return fair_mean,fair_var

def OM_unfair_fix_N_one_model(T,N,P0,P1,P2,R,alpha,beta,max_Q,nb_repeats=10,true_value=True,correct_prior=True):
    if correct_prior:
        candidate = generate_candidate(N)
    else:
        candidate = generate_candidate(N,portion=0.5,prior0=[1,1],prior1=[2,2],true_prior0=[1,1],true_prior1=[3,1])
    perfs = []
    for _ in range(nb_repeats):
        res_OM = OM_unfair(candidate,T,N,P0,P1,P2,R,alpha,beta,max_Q)
        perf = perf_measure(res_OM,beta,true_value)
        perfs.append(perf)
    return np.mean(perfs)

def OM_unfair_fix_N_many_models(T,N,P0,P1,P2,R,alpha,beta,max_Q,nb_repeats=10,nb_models=160,true_value=True,correct_prior=True):
    perf_OM = Parallel(n_jobs=-1)(delayed(OM_unfair_fix_N_one_model)(T,N,P0,P1,P2,R,alpha,beta,max_Q,nb_repeats, \
                                                              true_value,correct_prior) for _ in tqdm(range(nb_models)))
    OM_mean,OM_var = compute_mean_and_var(perf_OM)
    return OM_mean,OM_var

def OM_fix_N_one_model(T,N,P0,P1,P2,R,alpha,gamma0,gamma1,beta,max_Q,nb_repeats=10,true_value=True,correct_prior=True):
    if correct_prior:
        candidate = generate_candidate(N)
    else:
        candidate = generate_candidate(N,portion=0.5,prior0=[1,1],prior1=[2,2],true_prior0=[1,1],true_prior1=[3,1])
    perfs = []
    for _ in range(nb_repeats):
        res_OM = OM_fair(candidate,T,N,P0,P1,P2,R,alpha,gamma0,gamma1,beta,max_Q)
        perf = perf_measure(res_OM,beta,true_value)
        perfs.append(perf)
    return np.mean(perfs)

def OM_fix_N_many_models(T,N,P0,P1,P2,R,alpha,gamma0,gamma1,beta,max_Q,nb_repeats=10,nb_models=160,true_value=True,correct_prior=True):
    perf_OM = Parallel(n_jobs=-1)(delayed(OM_fix_N_one_model)(T,N,P0,P1,P2,R,alpha,gamma0,gamma1,beta,max_Q,nb_repeats, \
                                                              true_value,correct_prior) for _ in tqdm(range(nb_models)))
    OM_mean,OM_var = compute_mean_and_var(perf_OM)
    return OM_mean,OM_var

