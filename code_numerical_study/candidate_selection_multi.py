from scipy.stats import binom
import pulp as pulp
from pulp import *
import copy
import numpy as np
from colorama import Fore, Back, Style
import random

"""
A candidate is modeled as a real number $0 \le p \le 1$, which indicates her probability of success to solve a question (or task).

$N$ candidates come from two groups. One group with $N*(portion)$ candidates is generated from beta(1,1), another group with $N*(1-portion)$ candidates is generated from beta(2,2). 

At each questioning round we can ask a candidate zero, one or two questions. $Max\_Q$ is the maximum accumulated number of questions that we can ask on a single candidate during the whole process. 

At each questioning round, we have a time budget constraint of $N*\alpha$. Asking a candidate zero question consumes nothing, asking a candidate one question comsumes $1$ unit of time, while asking a candidate two questions consumes $1.5$ units of time. This should be interpreted as asking a single candidate two questions takes less time than asking them seperately on two different candidates

We also add the fairness constraints, which amongs to consume no more than $N*\gamma_g$ time budget in group $g$ at each round. Typically $\gamma_g$ is chosen to be slightly larger than $\alpha/2$.

$T$ is the total number of questioning rounds. At a final selection round, we admit $N*\beta$ of candidates. Our goal is to design a policy so as to maximize the expected total quality of the selected candidates in the end (i.e. sum of the $p$'s).

We call this the "candidate selection problem with fairness constraints". We evaluate the LP-update policy on this problem. 

It is also interesting to consider the exact problem WITHOUT the fairness constraits, and compare the performance of the LP-update policy under these two senarios, to see the effect of adding fairness constraints on this candidate selection problem.

"""

# Might be used to define more general budget consumption functions
def budget(a):
    if a == 0:
        return 0
    elif a == 1:
        return 1
    elif a == 2:
        return 1.5
# count the total number of possible state couples (x,y) if we start in (1,1) and a maximum number of max_Q questoins can be asked.
def total_states(max_Q):
    nb = int(0)
    for t in range(max_Q+1):
        nb += 1 + t
    return nb
# enumerate all the possible state couples (x,y)
# Attention: the number starts with 1, not 0!
def state_to_number(x,y):
    time_zone = x + y - 2
    if time_zone == 0:
        return 1
    nb = int(0)
    for i in range(time_zone):
        nb += i + 1
    nb += x
    return nb
def number_to_state(nb):
    time_zone = 0
    while nb - (time_zone+1) > 0:
        nb -= (time_zone+1)
        time_zone += 1
    x = nb
    y = 2 + time_zone - x
    return x,y
# The total number of states is total_states(max_Q+2), since group 1 starts in (2,2)!
def candidate_to_states(candidate,max_Q):
    n = total_states(max_Q+2)
    group = range(0,2)
    state = range(0,n)
    states = np.zeros((len(group),len(state)),dtype=int)
    for g in group:
        for item in candidate[g]:
            x,y = item[1]
            nb = state_to_number(x,y)
            states[g,nb-1] += 1
    return states

def construct_P(max_Q,action):
    n = total_states(max_Q+2)
    if action == 0:
        return [np.identity(n),np.identity(n)]
    P = np.zeros((2,n,n))
    for g in range(2):
        for i in range(n):
            x,y = number_to_state(i+1)
            if (x+y-2) - 2*g + action <= max_Q:
                if action == 1:
                    pmf = [x/(x+y),y/(x+y)]
                elif action == 2:
                    pmf = [x*(1+x)/((x+y)*(1+x+y)),2*x*y/((x+y)*(1+x+y)),y*(1+y)/((x+y)*(1+x+y))]
                for k in range(action+1):
                    j = state_to_number(x+action-k,y+k)
                    P[g,i,j-1] = pmf[k]
            # set the boundary effect
            else:
                P[g,i,i] = 1
    return P

def construct_R(max_Q):
    n = total_states(max_Q+2)
    R = np.zeros(n)
    for i in range(n):
        x,y = number_to_state(i+1)
        R[i] = x/(x+y)
    return R

def generate_candidate(N,portion=0.5,prior0=[1,1],prior1=[2,2],true_prior0=[1,1],true_prior1=[2,2]):
    candidate0 = []
    candidate1 = []
    for _ in range(int(N*portion)):
        candidate0.append([np.random.beta(true_prior0[0],true_prior0[1]),prior0])
    for _ in range(N-int(N*portion)):
        candidate1.append([np.random.beta(true_prior1[0],true_prior1[1]),prior1])
    return np.array([candidate0,candidate1],dtype=object)

def right_inverse(P):
    return np.matmul(np.transpose(P),np.linalg.inv(np.matmul(P,np.transpose(P))))
# compute Y = pi(MN)

def construct_Y_from_pi(MN,y,inverse_matrix,U_plus,nb_eq):
    n = np.shape(y)[1]
    group = range(0,2)
    state = range(0,n)
    action = range(0,3)
    Y = np.zeros((len(group),len(state),len(action)))
    m = np.zeros((len(group),len(state)))
    for g in group:
        for s in state:
            m[g,s] = sum(y[g,s,a] for a in action)
    nb_row,nb_column = np.shape(inverse_matrix)
    vector = np.zeros(nb_column)
    for i,item in enumerate(U_plus):
        g,s = item[0]
        vector[i+nb_eq] = MN[g,s] - m[g,s]
    res_vect = np.dot(inverse_matrix,vector)
    # res_vect is of length nb_row
    count = 0
    for item in U_plus:
        g,s = item[0]
        positive_action = item[1]
        for a in positive_action:
            Y[g,s,a] = y[g,s,a] + res_vect[count]
            count += 1
    return Y

def first_positive_index(mylist):
    for i in range(len(mylist)):
        if mylist[i] > 1e-6:
            return i
        
def simulate_one_candidate(x,y,p,action):
    if action == 0:
        return x,y
    k = np.random.binomial(action,p)
    x += k
    y += action - k
    return x,y

# NYN = N*YN, states = N*MN
def apply_NYN_to_candidate(NYN,candidate):
    next_candidate = copy.deepcopy(candidate)
    plan = copy.deepcopy(NYN)
    n = np.shape(NYN)[1]
    group = range(0,2)
    state = range(0,n)
    action = range(0,3)
    for g in group:
        for i,item in enumerate(candidate[g]):
            p = item[0]
            x,y = item[1]
            nb = state_to_number(x,y)-1
            action_choice = first_positive_index(plan[g,nb])
            if action_choice != None:
                xx,yy = simulate_one_candidate(x,y,p,action_choice)
                next_candidate[g,i][1] = [xx,yy]
                plan[g,nb,action_choice] -= 1
    return next_candidate
def perf_measure(candidate,beta,true_value=False):
    whole_candidate = []
    for i,groups in enumerate(candidate):
        whole_candidate.extend(groups)
    N = len(whole_candidate)
    S = int(round(beta*N))
    true_p = np.zeros(N)
    estimate_p = np.zeros(N)
    for i in range(N):
        p = whole_candidate[i][0]
        x,y = whole_candidate[i][1]
        true_p[i] = p
        estimate_p[i] = x/(x+y)
    estimate_order = np.argsort(-estimate_p)
    estimate = 0.
    if true_value:
        for i in range(S):
            estimate += true_p[estimate_order[i]]
    else:
        for i in range(S):
            estimate += estimate_p[estimate_order[i]]
    return estimate/S

# since the rounding using mixed integer programming is easy to solve, this simple rounding function is NOT recommanded to use.
def rounding_simple(states,N,Y):
    n = len(states[0])
    group = range(0,2)
    state = range(0,n)
    action = range(0,3)
    NYN = np.zeros((len(group),len(state),len(action)),dtype=int)
    floor = np.zeros((len(group),len(state),len(action)),dtype=int)
    ns = np.zeros((len(group),len(state)),dtype=int)
    for g in group:
        for s in state:
            for a in action:
                floor[g,s,a] = int(N*Y[g,s,a])
                NYN[g,s,a] = floor[g,s,a]
    for g in group:
        for s in state:
            ns[g,s] = states[g,s] - sum(floor[g,s,a] for a in action)
    for g in group:
        for s in state:
            NYN[g,s,0] += ns[g,s]
    return NYN

def solve_lp_WCMDP_unfair(T,init,P0,P1,P2,R,alpha,beta):
    # set up the parameters
    n = len(R)
    P = [P0,P1,P2]
    horizon = range(0,T+1)
    group = range(0,2)
    state = range(0,n)
    action = range(0,3)
    prob = LpProblem("LP1", LpMaximize)
    variables = LpVariable.dicts("y",(horizon,group,state,action),lowBound=0.,upBound=1.,cat='Continuous')
    # one constraint on the number of questions asked at each round
    for t in range(T):
        prob += lpSum([budget(a)*variables[t][g][s][a] for g in group for s in state for a in [1,2]]) <= alpha  
    # final selection at round T, action "1" means being selected
    prob += lpSum([variables[T][g][s][1] for g in group for s in state]) == beta
    # Markovian evolution
    for t in range(T):
        for g in group:
            for s in state:
                prob += lpSum([variables[t+1][g][s][a] for a in action]) == \
                            lpSum([variables[t][g][ss][a]*P[a][g][ss][s] for ss in state for a in action])
    # initial condition
    for g in group:
        for s in state:
            prob += lpSum([variables[0][g][s][a] for a in action]) == init[g][s]
    # objective    
    prob += lpSum([variables[T][g][s][1]*R[s] for g in group for s in state])
    # solve 
    prob.solve()
    rel = value(prob.objective)/beta 
    YT = np.zeros((len(horizon),len(group),len(state),len(action)))
    for t in horizon:
        for g in group:
            for s in state:
                for a in action:
                    V = variables[t][g][s][a]
                    YT[t,g,s,a] = V.varValue
    return rel,YT

def rounding_mix_integer_program_unfair(states,N,Y,alpha):
    # set up the parameters
    n = len(states[0])
    group = range(0,2)
    state = range(0,n)
    action = range(0,3)
    sign = range(0,2)
    z = np.zeros((len(group),len(state),len(action)))
    floor = np.zeros((len(group),len(state),len(action)),dtype=int)
    for g in group:
        for s in state:
            for a in action:
                floor[g,s,a] = int(N*Y[g,s,a])
                z[g,s,a] = N*Y[g,s,a] - floor[g,s,a]
                if abs(z[g,s,a]) < 1e-6:
                    z[g,s,a] = 0.
    ns = np.zeros((len(group),len(state)),dtype=int)
    for g in group:
        for s in state:
            ns[g,s] = states[g,s] - sum(floor[g,s,a] for a in action)
    # construct the mixed integer program
    prob = LpProblem("LP2", LpMinimize)
    Z = LpVariable.dict("Z",(group,state,action),lowBound=0,cat='Integer')
    W = LpVariable.dict("W",(sign,group,state,action),lowBound=0.,cat='Continuous')
    for g in group:
        for s in state:
            prob += lpSum([Z[g,s,a] for a in action]) == ns[g,s]
            for a in action:
                prob += W[1,g,s,a] - W[0,g,s,a] + z[g,s,a] == Z[g,s,a]        
    prob += lpSum([budget(a)*Z[g,s,a] for g in group for s in state for a in[1,2]]) <= \
                N*alpha - sum(budget(a)*floor[g,s,a] for g in group for s in state for a in [1,2])
    prob += lpSum([W[si,g,s,a] for si in sign for g in group for s in state for a in action])
    prob.solve()
    # construct NYN from the solution
    NYN = np.zeros((len(group),len(state),len(action)),dtype=int)
    for g in group:
        for s in state:
            for a in action:
                V = Z[g,s,a]
                NYN[g,s,a] = floor[g,s,a] + V.varValue
    return NYN

def check_feasible_condition_unfair(MN,Y,alpha):
    n = np.shape(Y)[1]
    group = range(0,2)
    state = range(0,n)
    action = range(0,3)
    for g in group:
        for s in state:
            for a in action:
                if  Y[g,s,a] < -1e-6:
                    return False
    if abs(1-np.sum(Y)) > 1e-6:
        return False
    for g in group:
        for s in state:
            if abs(MN[g,s] - sum(Y[g,s,a] for a in action)) > 1e-6:
                return False
    if sum(budget(a)*Y[g,s,a] for g in group for s in state for a in [1,2]) > alpha + 1e-6:
        return False
    return True

# return 0 if rank condition is not satisfied
# return a left inverse and U_plus if rank condition is satisfied
def check_rank_condition_unfair(y,alpha,detail=False):
    n = np.shape(y)[1]
    group = range(0,2)
    state = range(0,n)
    action = range(0,3)
    U_plus = []
    for g in group:
        for s in state:
            if sum(y[g,s,a] for a in action) > 1e-6:
                positive_action = []
                for a in action:
                    if y[g,s,a] > 1e-6:
                        positive_action.append(a)
                U_plus.append([[g,s],positive_action])
    if detail:
        print("U_plus is "+str(U_plus)+" with length "+str(len(U_plus)))
    matrix = []
    # construct the D= matrix 
    # check if the constraint on the number of questions is saturated
    if abs(sum(budget(a)*y[g,s,a] for g in group for s in state for a in [1,2]) - alpha) < 1e-6:
        line = []
        for item in U_plus:
            positive_action = item[1]
            line.extend([budget(a) for a in positive_action])
        matrix.append(line)
    nb_eq = len(matrix)
    if nb_eq >= 1:
        nb_column = len(matrix[0])
    else:
        nb_column = 0
        for item in U_plus:
            nb_column += len(item[1])
    # construct the C matrix 
    start_position = 0
    for i in range(len(U_plus)):
        line = np.zeros(nb_column)
        nb_positive_action = len(U_plus[i][1])
        line[start_position:start_position+nb_positive_action] = 1
        matrix.append(list(line))
        start_position += nb_positive_action
    nb_row,nb_column = np.shape(matrix)
    rank = np.linalg.matrix_rank(matrix)
    if detail:
        print("The dimension of the matrix is "+str(nb_row)+", "+str(nb_column))
        print("The rank of the matrix is "+str(rank))
    if nb_row == rank:
        inverse_matrix = right_inverse(np.array(matrix))
        return inverse_matrix,U_plus,nb_eq
    else: 
        return 0
    
def random_selection_one_step(candidate,alpha):
    g0 = len(candidate[0]) 
    g1 =  len(candidate[1])
    N = g0 + g1
    budget_left = N*alpha
    not_selected_set = set(range(0,N))
    next_candidate = copy.deepcopy(candidate)
    while budget_left >= budget(1) + 1e-6:
        select_number = random.sample(not_selected_set,1)[0]
        if select_number < g0:
            g = 0
            p = candidate[g,select_number][0]
            x,y = candidate[g,select_number][1]
        else: 
            g = 1
            p = candidate[g,select_number-g0][0]
            x,y = candidate[g,select_number-g0][1]
        if budget_left >= budget(2) + 1e-6:
            if np.random.uniform() < 0.5:
                select_action = 1
            else: 
                select_action = 2
        else:
            select_action = 1
        xx,yy = simulate_one_candidate(x,y,p,select_action)
        if g == 0:
            next_candidate[g,select_number][1] = [xx,yy]
        else:
            next_candidate[g,select_number-g0][1] = [xx,yy]
        budget_left -= budget(select_action)
        not_selected_set.remove(select_number)
    return next_candidate

def random_selection_unfair(candidate,current_horizon,alpha,detail=False):
    if detail:
        print("Candidate is "+str(candidate)+" at horizon "+str(current_horizon))
        print()
    if current_horizon == 0:
        return candidate
    else:
        candidate = random_selection_one_step(candidate,alpha)
        current_horizon -= 1
        return random_selection_unfair(candidate,current_horizon,alpha,detail)
    
# This function is defined only for a specific parameter, and is only used to provide a heuristic for comparison with the LP-update policy
def interview_all_with_two_questions_heuristic(candidate,T=10,alpha=0.3,detail=False):
    g0 = len(candidate[0]) 
    g1 =  len(candidate[1])
    N = g0 + g1
    nb = int(N*0.2)
    next_candidate = copy.deepcopy(candidate)
    for t in range(T):
        if detail:
            print("Candidate is "+str(next_candidate)+" at horizon "+str(T-t))
            print()
        num = t % 5
        for i in range(nb*num,nb*(num+1)):
            if i < g0:
                g = 0
                p = next_candidate[g,i][0]
                x,y = next_candidate[g,i][1]
                xx,yy = simulate_one_candidate(x,y,p,2)
                next_candidate[g,i][1] = [xx,yy]
            else:
                g = 1
                p = next_candidate[g,i-g0][0]
                x,y = next_candidate[g,i-g0][1]
                xx,yy = simulate_one_candidate(x,y,p,2)
                next_candidate[g,i-g0][1] = [xx,yy]
    if detail:
        print("Candidate is "+str(next_candidate)+" at horizon "+str(0))
    return next_candidate

def LP_update_recursive_unfair(candidate,current_horizon,N,P0,P1,P2,R,alpha,beta,max_Q,detail=False):
    if current_horizon == 0:
        return candidate
    else:
        if detail:
            print(Fore.RED+"current horizon is "+str(current_horizon)+", we need to solve the LP!")
            print()
            print(Style.RESET_ALL)
        current_states = candidate_to_states(candidate,max_Q)
        initial_configuration = current_states/N
        rel,YT = solve_lp_WCMDP_unfair(current_horizon,initial_configuration,P0,P1,P2,R,alpha,beta)
        Y = YT[0]
        NYN = rounding_mix_integer_program_unfair(current_states,N,Y,alpha)
        #NYN = rounding_simple(current_states,N,Y)
        candidate = apply_NYN_to_candidate(NYN,candidate)
        current_horizon -= 1
        for t in range(current_horizon):
            current_states = candidate_to_states(candidate,max_Q)
            y = YT[t+1]
            ans = check_rank_condition_unfair(y,alpha,detail)
            # check rank condition
            if ans != 0:
                inverse_matrix,U_plus,nb_eq = ans
                MN = current_states/N    
                Y = construct_Y_from_pi(MN,y,inverse_matrix,U_plus,nb_eq)
                # check feasible condition
                feasible = check_feasible_condition_unfair(MN,Y,alpha)
                if feasible:
                    if detail:
                        print(Fore.RED+"current horizon is "+str(current_horizon)+", we get a feasible action from pi!")
                        print()
                        print(Style.RESET_ALL)
                    NYN = rounding_mix_integer_program_unfair(current_states,N,Y,alpha)
                    #NYN = rounding_simple(current_states,N,Y)
                    candidate = apply_NYN_to_candidate(NYN,candidate)
                    current_horizon -= 1
                else:
                    if detail:
                        print("rank ok but not feasible")
                        print()
                    break
            else:
                if detail:
                    print("rank not ok")
                    print()
                break
        return LP_update_recursive_unfair(candidate,current_horizon,N,P0,P1,P2,R,alpha,beta,max_Q,detail)
    
def solve_lp_WCMDP(T,init,P0,P1,P2,R,alpha,gamma0,gamma1,beta):
    # set up the parameters
    n = len(R)
    P = [P0,P1,P2]
    horizon = range(0,T+1)
    group = range(0,2)
    state = range(0,n)
    action = range(0,3)
    prob = LpProblem("LP1", LpMaximize)
    variables = LpVariable.dicts("y",(horizon,group,state,action),lowBound=0.,upBound=1.,cat='Continuous')
    # one constraint on the number of questions asked at each round
    for t in range(T):
        prob += lpSum([budget(a)*variables[t][g][s][a] for g in group for s in state for a in [1,2]]) <= alpha  
    # two constraints on fairness
    for t in range(T):
        prob += lpSum([budget(a)*variables[t][0][s][a] for s in state for a in [1,2]]) <= gamma0
    for t in range(T):
        prob += lpSum([budget(a)*variables[t][1][s][a] for s in state for a in [1,2]]) <= gamma1
    # final selection at round T, action "1" means being selected
    prob += lpSum([variables[T][g][s][1] for g in group for s in state]) == beta
    # Markovian evolution
    for t in range(T):
        for g in group:
            for s in state:
                prob += lpSum([variables[t+1][g][s][a] for a in action]) == \
                            lpSum([variables[t][g][ss][a]*P[a][g][ss][s] for ss in state for a in action])
    # initial condition
    for g in group:
        for s in state:
            prob += lpSum([variables[0][g][s][a] for a in action]) == init[g][s]
    # objective    
    prob += lpSum([variables[T][g][s][1]*R[s] for g in group for s in state])
    # solve 
    prob.solve()
    rel = value(prob.objective)/beta 
    YT = np.zeros((len(horizon),len(group),len(state),len(action)))
    for t in horizon:
        for g in group:
            for s in state:
                for a in action:
                    V = variables[t][g][s][a]
                    YT[t,g,s,a] = V.varValue
    return rel,YT

def rounding_mix_integer_program(states,N,Y,alpha,gamma0,gamma1):
    # set up the parameters
    n = len(states[0])
    group = range(0,2)
    state = range(0,n)
    action = range(0,3)
    sign = range(0,2)
    z = np.zeros((len(group),len(state),len(action)))
    floor = np.zeros((len(group),len(state),len(action)),dtype=int)
    for g in group:
        for s in state:
            for a in action:
                floor[g,s,a] = int(N*Y[g,s,a])
                z[g,s,a] = N*Y[g,s,a] - floor[g,s,a]
                if abs(z[g,s,a]) < 1e-6:
                    z[g,s,a] = 0.
    ns = np.zeros((len(group),len(state)),dtype=int)
    for g in group:
        for s in state:
            ns[g,s] = states[g,s] - sum(floor[g,s,a] for a in action)
    # construct the mixed integer program
    prob = LpProblem("LP2", LpMinimize)
    Z = LpVariable.dict("Z",(group,state,action),lowBound=0,cat='Integer')
    W = LpVariable.dict("W",(sign,group,state,action),lowBound=0.,cat='Continuous')
    for g in group:
        for s in state:
            prob += lpSum([Z[g,s,a] for a in action]) == ns[g,s]
            for a in action:
                prob += W[1,g,s,a] - W[0,g,s,a] + z[g,s,a] == Z[g,s,a]        
    prob += lpSum([budget(a)*Z[g,s,a] for g in group for s in state for a in [1,2]]) <= \
                N*alpha - sum(budget(a)*floor[g,s,a] for g in group for s in state for a in [1,2])
    prob += lpSum([budget(a)*Z[0,s,a] for s in state for a in [1,2]]) <= N*gamma0 - sum(budget(a)*floor[0,s,a] for s in state for a in [1,2])
    prob += lpSum([budget(a)*Z[1,s,a] for s in state for a in [1,2]]) <= N*gamma1 - sum(budget(a)*floor[1,s,a] for s in state for a in [1,2])
    prob += lpSum([W[si,g,s,a] for si in sign for g in group for s in state for a in action])
    prob.solve()
    # construct NYN from the solution
    NYN = np.zeros((len(group),len(state),len(action)),dtype=int)
    for g in group:
        for s in state:
            for a in action:
                V = Z[g,s,a]
                NYN[g,s,a] = floor[g,s,a] + V.varValue
    return NYN

def check_feasible_condition(MN,Y,alpha,gamma0,gamma1):
    n = np.shape(Y)[1]
    group = range(0,2)
    state = range(0,n)
    action = range(0,3)
    for g in group:
        for s in state:
            for a in action:
                if  Y[g,s,a] < -1e-6:
                    return False
    if abs(1-np.sum(Y)) > 1e-6:
        return False
    for g in group:
        for s in state:
            if abs(MN[g,s] - sum(Y[g,s,a] for a in action)) > 1e-6:
                return False
    if sum(budget(a)*Y[g,s,a] for g in group for s in state for a in [1,2]) > alpha + 1e-6:
        return False
    if sum(budget(a)*Y[0,s,a] for s in state for a in [1,2]) > gamma0 + 1e-6:
        return False
    if sum(budget(a)*Y[1,s,a] for s in state for a in [1,2]) > gamma1 + 1e-6:
        return False
    return True

# return 0 if rank condition is not satisfied
# return a left inverse and U_plus if rank condition is satisfied
def check_rank_condition(y,alpha,gamma0,gamma1,detail=False):
    n = np.shape(y)[1]
    group = range(0,2)
    state = range(0,n)
    action = range(0,3)
    U_plus = []
    for g in group:
        for s in state:
            if sum(y[g,s,a] for a in action) > 1e-6:
                positive_action = []
                for a in action:
                    if y[g,s,a] > 1e-6:
                        positive_action.append(a)
                U_plus.append([[g,s],positive_action])
    if detail:
        print("U_plus is "+str(U_plus)+" with length "+str(len(U_plus)))
    matrix = []
    saturate_constraint_name = []
    # construct the D= matrix 
    # check if the constraint on the number of questions is saturated
    if abs(sum(budget(a)*y[g,s,a] for g in group for s in state for a in [1,2]) - alpha) < 1e-6:
        line = []
        for item in U_plus:
            positive_action = item[1]
            line.extend([budget(a) for a in positive_action])
        matrix.append(line)
        saturate_constraint_name.append('total budget')
    # check if the two constraints on fairness are saturated
    if abs(sum(budget(a)*y[0,s,a] for s in state for a in [1,2]) - gamma0) < 1e-6:
        line = []
        for item in U_plus:
            g,s = item[0]
            positive_action = item[1]
            if g == 0:
                line.extend([budget(a) for a in positive_action])
            else:
                line.extend([0]*len(positive_action))
        matrix.append(line)
        saturate_constraint_name.append('group 0')
    if abs(sum(budget(a)*y[1,s,a] for s in state for a in [1,2]) - gamma1) < 1e-6:
        line = []
        for item in U_plus:
            g,s = item[0]
            positive_action = item[1]
            if g == 1:
                line.extend([budget(a) for a in positive_action])
            else:
                line.extend([0]*len(positive_action))
        matrix.append(line)
        saturate_constraint_name.append('group 1')
    nb_eq = len(matrix)
    if detail:
        print(str(nb_eq)+" constraint(s) saturated: "+str(saturate_constraint_name))
    if nb_eq >= 1:
        nb_column = len(matrix[0])
    else:
        nb_column = 0
        for item in U_plus:
            nb_column += len(item[1])
    # construct the C matrix 
    start_position = 0
    for i in range(len(U_plus)):
        line = np.zeros(nb_column)
        nb_positive_action = len(U_plus[i][1])
        line[start_position:start_position+nb_positive_action] = 1
        matrix.append(list(line))
        start_position += nb_positive_action
    nb_row,nb_column = np.shape(matrix)
    rank = np.linalg.matrix_rank(matrix)
    if detail:
        print("The dimension of the matrix is "+str(nb_row)+", "+str(nb_column))
        print("The rank of the matrix is "+str(rank))
    if nb_row == rank:
        inverse_matrix = right_inverse(np.array(matrix))
        return inverse_matrix,U_plus,nb_eq
    else: 
        return 0

def LP_update_recursive(candidate,current_horizon,N,P0,P1,P2,R,alpha,gamma0,gamma1,beta,max_Q,detail=False):
    if current_horizon == 0:
        return candidate
    else:
        if detail:
            print(Fore.RED+"current horizon is "+str(current_horizon)+", we need to solve the LP!")
            print()
            print(Style.RESET_ALL)
        current_states = candidate_to_states(candidate,max_Q)
        initial_configuration = current_states/N
        rel,YT = solve_lp_WCMDP(current_horizon,initial_configuration,P0,P1,P2,R,alpha,gamma0,gamma1,beta)
        Y = YT[0]
        NYN = rounding_mix_integer_program(current_states,N,Y,alpha,gamma0,gamma1)
        #NYN = rounding_simple(current_states,N,Y)
        candidate = apply_NYN_to_candidate(NYN,candidate)
        current_horizon -= 1
        for t in range(current_horizon):
            current_states = candidate_to_states(candidate,max_Q)
            y = YT[t+1]
            ans = check_rank_condition(y,alpha,gamma0,gamma1,detail)
            # check rank condition
            if ans != 0:
                inverse_matrix,U_plus,nb_eq = ans
                MN = current_states/N    
                Y = construct_Y_from_pi(MN,y,inverse_matrix,U_plus,nb_eq)
                # check feasible condition
                feasible = check_feasible_condition(MN,Y,alpha,gamma0,gamma1)
                if feasible:
                    if detail:
                        print(Fore.RED+"current horizon is "+str(current_horizon)+", we get a feasible action from pi!")
                        print()
                        print(Style.RESET_ALL)
                    NYN = rounding_mix_integer_program(current_states,N,Y,alpha,gamma0,gamma1)
                    #NYN = rounding_simple(current_states,N,Y)
                    candidate = apply_NYN_to_candidate(NYN,candidate)
                    current_horizon -= 1
                else:
                    if detail:
                        print("rank ok but not feasible")
                        print()
                    break
            else:
                if detail:
                    print("rank not ok")
                    print()
                break
        return LP_update_recursive(candidate,current_horizon,N,P0,P1,P2,R,alpha,gamma0,gamma1,beta,max_Q,detail)   

def compute_mean_and_var(mylist):
    return np.mean(mylist),2*np.std(mylist)/np.sqrt(len(mylist)-1)

def sampling(proba_line):
    n = len(proba_line)
    seed = np.random.uniform(0,1)
    position = 0
    while sum(proba_line[0:position+1]) < seed:
        position += 1
    return position

# Implemantation of the occupation measure policy

def occupation_measure_from_LP(y):
    n = np.shape(y)[1]
    group = range(0,2)
    state = range(0,n)
    action = range(0,3)
    OM = np.zeros((len(group),len(state)),dtype=object)
    for g in group:
        for s in state:
            if sum(y[g,s,a] for a in action) > 1e-6:
                acts_proba = [y[g,s,a] for a in action]
                acts_proba /= sum(acts_proba)
                OM[g,s] = acts_proba   
            else:
                OM[g,s] = [1.,0.,0.]
    return OM

def OM_unfair_apply_to_candidate(OM,candidate,N,alpha,max_Q):
    states = candidate_to_states(candidate,max_Q)
    n = len(states[0])
    group = range(0,2)
    state = range(0,n)
    action = range(0,3)
    budget_left = N*alpha
    # initialize NYN
    NYN = np.zeros((len(group),len(state),len(action)),dtype=int)
    for g in group:
        for s in state:
            NYN[g,s,0] = states[g,s]
    # sample each candidate an action according to the occupation measure, as long as there is budget left
    for g in group:
        for item in candidate[g]:
            x,y = item[1]
            nb = state_to_number(x,y)
            acts_proba = OM[g,nb-1]
            action_choice = sampling(acts_proba)
            consumption = budget(action_choice)
            if budget_left - consumption > 1e-6:
                NYN[g,nb-1,0] -= 1
                NYN[g,nb-1,action_choice] += 1
                budget_left -= consumption
    return NYN

def OM_apply_to_candidate(OM,candidate,N,alpha,gamma0,gamma1,max_Q):
    states = candidate_to_states(candidate,max_Q)
    n = len(states[0])
    group = range(0,2)
    state = range(0,n)
    action = range(0,3)
    budget_left = N*alpha
    fairness = [N*gamma0,N*gamma1]
    # initialize NYN
    NYN = np.zeros((len(group),len(state),len(action)),dtype=int)
    for g in group:
        for s in state:
            NYN[g,s,0] = states[g,s]
    # sample each candidate an action according to the occupation measure, as long as there is budget left
    for g in group:
        for item in candidate[g]:
            x,y = item[1]
            nb = state_to_number(x,y)
            acts_proba = OM[g,nb-1]
            action_choice = sampling(acts_proba)
            consumption = budget(action_choice)
            if budget_left - consumption > 1e-6 and fairness[g] - consumption > 1e-6:
                NYN[g,nb-1,0] -= 1
                NYN[g,nb-1,action_choice] += 1
                budget_left -= consumption
                fairness[g] -= consumption
    return NYN

def OM_unfair(candidate,current_horizon,N,P0,P1,P2,R,alpha,beta,max_Q):
    current_states = candidate_to_states(candidate,max_Q)
    initial_configuration = current_states/N
    rel,YT = solve_lp_WCMDP_unfair(current_horizon,initial_configuration,P0,P1,P2,R,alpha,beta)
    for t in range(current_horizon):
        OM = occupation_measure_from_LP(YT[t])
        NYN = OM_unfair_apply_to_candidate(OM,candidate,N,alpha,max_Q)
        candidate = apply_NYN_to_candidate(NYN,candidate)
    return candidate

def OM_fair(candidate,current_horizon,N,P0,P1,P2,R,alpha,gamma0,gamma1,beta,max_Q):
    current_states = candidate_to_states(candidate,max_Q)
    initial_configuration = current_states/N
    rel,YT = solve_lp_WCMDP(current_horizon,initial_configuration,P0,P1,P2,R,alpha,gamma0,gamma1,beta)
    for t in range(current_horizon):
        OM = occupation_measure_from_LP(YT[t])
        NYN = OM_apply_to_candidate(OM,candidate,N,alpha,gamma0,gamma1,max_Q)
        candidate = apply_NYN_to_candidate(NYN,candidate)
    return candidate
