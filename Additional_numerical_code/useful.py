import numpy as np
import pulp as pulp
from pulp import *
import random
from tqdm import tqdm

# generate a full rank dense n-dimensional transition matrix
def dense_matrix(n):
    P = np.zeros((n,n))
    for i in range(n):
        for j in range(n):
            P[i,j] = np.random.exponential()
    for i in range(n):
        s = np.sum(P[i])
        for j in range(n):
            P[i,j] = P[i,j]/s
    return P

# generate a tri-diagonal n dimensional transition matrix
def tri_matrix(n):
    P = np.zeros((n,n))
    P[0,0] = np.random.exponential()
    P[0,1] = np.random.exponential()
    P[n-1,n-2] = np.random.exponential()
    P[n-1,n-1] = np.random.exponential()
    for i in range(1,n-1):
        P[i,i-1] = np.random.exponential()
        P[i,i] = np.random.exponential()
        P[i,i+1] = np.random.exponential()
    for i in range(n):
        ss = np.sum(P[i])
        for j in range(n):
            P[i,j] /= ss
    return P

def sparse_matrix(n):
    P = np.zeros((n, n))
    for i in range(n):
        cols = np.random.choice(n, 2, replace=False)
        for j in cols:
            P[i, j] = np.random.exponential()
        P[i] /= P[i].sum()
    return P

def random_paras(n,J=3,A=3,dense=True):
    if dense:
        P0 = dense_matrix(n)
        P1 = dense_matrix(n)
        P2 = dense_matrix(n)
    else:
        P0 = tri_matrix(n)
        P1 = tri_matrix(n)
        P2 = tri_matrix(n)
    R0 = np.array([np.random.uniform(0,1) for i in range(n)])
    R1 = np.array([np.random.uniform(1,10) for i in range(n)])
    R2 = np.array([np.random.uniform(1,10) for i in range(n)])
    init = np.array([np.random.exponential() for i in range(n)])
    init /= sum(init)
    b = np.array([np.random.uniform(1,3) for j in range(J)])
    D = np.zeros((J,A,n))
    for j in range(J):
        for a in range(A):
            for s in range(n):
                if a != 0:
                    D[j,a,s] = np.random.uniform(1,4)
                else:
                    D[j,a,s] = 0
    return P0,P1,P2,R0,R1,R2,init,D,b

def give_states(N,init):
    n = len(init)
    states = np.zeros(n,dtype=int)
    for i in range(n):
        ss = round(N*init[i])
        if ss + sum(states) <= N:
            states[i] = ss
        else:
            states[i] = ss-1
            break
    states[-1] = N - sum(states[:n-1])
    return states

def get_rel(P0,P1,P2,R0,R1,R2,init,D,b,T):
    n = len(R0)
    J = len(b)
    P = [P0,P1,P2]
    R = [R0,R1,R2]
    A = len(P)
    action = range(0,A)
    state = range(0,n)
    horizon = range(0,T)
    resource = range(0,J)
    prob = LpProblem("LP1", LpMaximize)
    variables = LpVariable.dicts("Y",(horizon,action,state),lowBound=0., upBound=1.)
    # resource constraints 
    for t in horizon:
        for j in resource:
            prob += lpSum([variables[t][a][s]*D[j,a,s] for s in state for a in action]) <= b[j]
    # Markov state evolution
    for t in range(0,T-1):
        for s in state:
            prob += lpSum(variables[t+1][a][s] for a in action) == lpSum([variables[t][a][ss]*P[a][ss][s] for a in action for ss 
                                                                          in state])
    # initial condition        
    for s in state:
        prob += lpSum(variables[0][a][s] for a in action) == init[s]
    # objective    
    prob += lpSum([variables[t][a][s]*R[a][s] for t in horizon for a in action for s in state])
    
    prob.solve()
    
    return value(prob.objective)

def get_y_first_step(P0,P1,P2,R0,R1,R2,init,D,b,T):
    n = len(R0)
    J = len(b)
    P = [P0,P1,P2]
    R = [R0,R1,R2]
    A = len(P)
    action = range(0,A)
    state = range(0,n)
    horizon = range(0,T)
    resource = range(0,J)
    prob = LpProblem("LP1", LpMaximize)
    variables = LpVariable.dicts("Y",(horizon,action,state),lowBound=0., upBound=1.)
    # resource constraints 
    for t in horizon:
        for j in resource:
            prob += lpSum([variables[t][a][s]*D[j,a,s] for s in state for a in action]) <= b[j]
    # Markov state evolution
    for t in range(0,T-1):
        for s in state:
            prob += lpSum(variables[t+1][a][s] for a in action) == lpSum([variables[t][a][ss]*P[a][ss][s] for a in action for ss 
                                                                          in state])
    # initial condition        
    for s in state:
        prob += lpSum(variables[0][a][s] for a in action) == init[s]
    # objective    
    prob += lpSum([variables[t][a][s]*R[a][s] for t in horizon for a in action for s in state])
    
    prob.solve()
    
    y = np.zeros((A,n))
    for a in action:
        for s in state:
            V = variables[0][a][s]
            v = V.varValue
            y[a,s] = v
    return y

def get_Y_first_step(states,y):
    A = len(y)
    n = len(states)
    N = sum(states)
    Y = np.zeros((A,n),dtype=int)
    action = range(0,A)
    state = range(0,n)
    for a in range(1,A):
        for s in state:
            Y[a,s] = int(N*y[a,s])
    for s in state:
        Y[0,s] = states[s] - sum(Y[1:A,s])
    return Y

def get_probas(P0,P1,P2,R0,R1,R2,init,D,b,T):
    n = len(R0)
    J = len(b)
    P = [P0,P1,P2]
    R = [R0,R1,R2]
    A = len(P)
    action = range(0,A)
    state = range(0,n)
    horizon = range(0,T)
    resource = range(0,J)
    prob = LpProblem("LP1", LpMaximize)
    variables = LpVariable.dicts("Y",(horizon,action,state),lowBound=0., upBound=1.)
    # resource constraints 
    for t in horizon:
        for j in resource:
            prob += lpSum([variables[t][a][s]*D[j,a,s] for s in state for a in action]) <= b[j]
    # Markov state evolution
    for t in range(0,T-1):
        for s in state:
            prob += lpSum(variables[t+1][a][s] for a in action) == lpSum([variables[t][a][ss]*P[a][ss][s] for a in action for ss 
                                                                          in state])
    # initial condition        
    for s in state:
        prob += lpSum(variables[0][a][s] for a in action) == init[s]
    # objective    
    prob += lpSum([variables[t][a][s]*R[a][s] for t in horizon for a in action for s in state])
    
    prob.solve()
    
    y = np.zeros((T,A,n))
    for t in horizon:
        for a in action:
            for s in state:
                V = variables[t][a][s]
                v = V.varValue
                y[t,a,s] = v
    probas = np.zeros((T,A,n))
    for t in horizon:
        for s in state:
            total = sum(y[t,:,s])
            if abs(total) <= 1e-7:
                for a in action:
                    probas[t,a,s] = 1/A
            else:
                for a in action:
                    probas[t,a,s] = y[t,a,s]/total
    return probas

def are_all_positive(numbers):
    # Check if all numbers in the array are positive
    for number in numbers:
        if number < 0:
            return False
    return True

def sampling(proba_line):
    n = len(proba_line)
    seed = np.random.uniform(0,1)
    position = 0
    while sum(proba_line[0:position+1]) < seed:
        position += 1
    return position

def get_Y_from_probas(states, D, b, probas_t):
    A = len(probas_t)
    N = sum(states)
    n = len(states)
    state = range(0,n)
    J = len(b)
    budget = N * b
    Y = np.zeros((A, n), dtype=int)

    # Create a list of states with their non-zero count in probas_t
    state_non_zero_counts = [(s, np.count_nonzero(probas_t[:, s] - 1)) for s in range(n)]

    # Sort states by the number of non-zero entries (prioritize states with a single 1)
    sorted_states = sorted(state_non_zero_counts, key=lambda x: x[1])

    # Iterate over states based on the sorted order
    for s, _ in sorted_states:
        for _ in range(states[s]):
            sample_action = sampling(probas_t[:, s])
            budget = budget - D[:, sample_action, s]
            if not are_all_positive(budget):
                break
            Y[sample_action, s] += 1
    for s in state:
        Y[0,s] += states[s] - sum(Y[:,s])
    return Y

def simulate_one_step(states,Y,P0,P1,P2,R0,R1,R2):
    n = len(states)
    A = len(Y)
    data = []
    action = range(0,A)
    state = range(0,n)
    P = [P0,P1,P2]
    R = [R0,R1,R2]
    reward = 0.
    for a in action:
        for s in state:
            data.append(np.random.multinomial(Y[a,s],P[a][s]))
            reward += Y[a,s]*R[a][s]
    return sum(data),reward/sum(states)

def occupation_measure_sim(states, T, P0, P1, P2, R0, R1, R2, D, b):
    N = sum(states)
    total_reward = 0.
    true_init = states/N
    probas = get_probas(P0,P1,P2,R0,R1,R2,true_init,D,b,T)
    for t in range(T):
        probas_t = probas[t]
        Y = get_Y_from_probas(states, D, b, probas_t)
        states, r = simulate_one_step(states, Y, P0, P1, P2, R0, R1, R2)
        total_reward += r
    return total_reward

def update_sim(states, T, P0, P1, P2, R0, R1, R2, D, b):
    N = sum(states)
    total_reward = 0.
    while T > 0:
        true_init = states / N
        y = get_y_first_step(P0, P1, P2, R0, R1, R2, true_init, D, b, T)
        Y = get_Y_first_step(states, y)
        states, r = simulate_one_step(states, Y, P0, P1, P2, R0, R1, R2)
        T -= 1
        total_reward += r
    return total_reward

def update_rounding_sim(states, T, P0, P1, P2, R0, R1, R2, D, b):
    N = sum(states)
    total_reward = 0.
    while T > 0:
        Y0 = get_Y_first_step_rounding(P0,P1,P2,R0,R1,R2,states,D,b,T)
        states, r = simulate_one_step(states, Y0, P0, P1, P2, R0, R1, R2)
        T -= 1
        total_reward += r
    return total_reward

def check_degeneracy_strong(P0,P1,P2,R0,R1,R2,init,D,b,T):
    n = len(R0)
    J = len(b)
    P = [P0, P1, P2]
    R = [R0, R1, R2]
    action = range(3)
    state = range(n)
    horizon = range(T)
    resource = range(J)
    prob = LpProblem("LP1", LpMaximize)
    variables = LpVariable.dicts("Y", (horizon, action, state), lowBound=0., upBound=1.)

    # Store constraints for later access
    constraints = {t: [] for t in horizon}

    # Resource constraints
    for t in horizon:
        for j in resource:
            constraint = lpSum([variables[t][a][s]*D[j,a,s] for s in state for a in action]) <= b[j]
            prob += constraint
            constraints[t].append(constraint)

    # Markov state evolution
    for t in range(0,T-1):
        for s in state:
            prob += lpSum(variables[t+1][a][s] for a in action) == lpSum([variables[t][a][ss]*P[a][ss][s] for a in action for ss 
                                                                          in state])
    # initial condition        
    for s in state:
        prob += lpSum(variables[0][a][s] for a in action) == init[s]
    # objective    
    prob += lpSum([variables[t][a][s]*R[a][s] for t in horizon for a in action for s in state])
    
    prob.solve()

    for t in horizon:
        J_star = 0
        S0 = list()
        for s in state:
            count = 0
            for a in action:
                V = variables[t][a][s]
                v = V.varValue
                if abs(v) > 5*1e-7:
                    count += 1
            if count > 1:
                S0.append(count)   
        for j, constraint in enumerate(constraints[t]):
            if constraint.pi != 0:  # Checking the shadow price
                J_star += 1
        lhs = J_star + len(S0)
        rhs = sum(S0)
        if lhs > rhs:
            return False
    return True

def check_degeneracy_weak(P0,P1,P2,R0,R1,R2,init,D,b,T):
    n = len(R0)
    J = len(b)
    P = [P0, P1, P2]
    R = [R0, R1, R2]
    action = range(3)
    state = range(n)
    horizon = range(T)
    resource = range(J)
    prob = LpProblem("LP1", LpMaximize)
    variables = LpVariable.dicts("Y", (horizon, action, state), lowBound=0., upBound=1.)

    # Store constraints for later access
    constraints = {t: [] for t in horizon}

    # Resource constraints
    for t in horizon:
        for j in resource:
            constraint = lpSum([variables[t][a][s]*D[j,a,s] for s in state for a in action]) <= b[j]
            prob += constraint
            constraints[t].append(constraint)

    # Markov state evolution
    for t in range(0,T-1):
        for s in state:
            prob += lpSum(variables[t+1][a][s] for a in action) == lpSum([variables[t][a][ss]*P[a][ss][s] for a in action for ss 
                                                                          in state])
    # initial condition        
    for s in state:
        prob += lpSum(variables[0][a][s] for a in action) == init[s]
    # objective    
    prob += lpSum([variables[t][a][s]*R[a][s] for t in horizon for a in action for s in state])
    
    prob.solve()

    for t in horizon:
        J_star = 0
        S0 = list()
        for s in state:
            count = 0
            for a in action:
                V = variables[t][a][s]
                v = V.varValue
                if abs(v) > 5*1e-7:
                    count += 1
            if count > 1:
                S0.append(count)   
        for j, constraint in enumerate(constraints[t]):
            if constraint.pi != 0:  # Checking the shadow price
                J_star += 1
        if not(J_star <= 1 and len(S0) >= 1):
            return False
    return True

def get_Y_first_step_rounding(P0,P1,P2,R0,R1,R2,states,D,b,T):
    n = len(R0)
    N = sum(states)
    J = len(b)
    P = [P0,P1,P2]
    R = [R0,R1,R2]
    A = len(P)
    action = range(0,A)
    state = range(0,n)
    horizon = range(1,T)
    resource = range(0,J)
    prob = LpProblem("LP1", LpMaximize)
    variables_int = LpVariable.dicts("Y0",(action,state),lowBound=0, cat='Integer')
    variables_real = LpVariable.dicts("Y",(horizon,action,state),lowBound=0.,cat='Continuous')
    # resource constraints 
    for j in resource:
        prob += lpSum([variables_int[a][s]*D[j,a,s] for s in state for a in action]) <= b[j]*N
    for t in horizon:
        for j in resource:
            prob += lpSum([variables_real[t][a][s]*D[j,a,s] for s in state for a in action]) <= b[j]*N
    # Markov state evolution
    if T > 1:
        for s in state:
            prob += lpSum(variables_real[1][a][s] for a in action) == lpSum([variables_int[a][ss]*P[a][ss][s] for a in action for ss in state])
        for t in range(1,T-1):
            for s in state:
                prob += lpSum(variables_real[t+1][a][s] for a in action) == lpSum([variables_real[t][a][ss]*P[a][ss][s] for a in action for ss in state])
    # initial condition        
    for s in state:
        prob += lpSum(variables_int[a][s] for a in action) == states[s]
    # objective    
    prob += lpSum([variables_int[a][s]*R[a][s] for a in action for s in state] + 
                  [variables_real[t][a][s]*R[a][s] for t in horizon for a in action for s in state])
    
    prob.solve()
    
    Y0 = np.zeros((A,n),dtype = int)
    for a in action:
        for s in state:
            V = variables_int[a][s]
            v = V.varValue
            Y0[a,s] = v    
    return Y0

def get_rel_rounding(P0,P1,P2,R0,R1,R2,states,D,b,T):
    n = len(R0)
    N = sum(states)
    J = len(b)
    P = [P0,P1,P2]
    R = [R0,R1,R2]
    A = len(P)
    action = range(0,A)
    state = range(0,n)
    horizon = range(1,T)
    resource = range(0,J)
    prob = LpProblem("LP1", LpMaximize)
    variables_int = LpVariable.dicts("Y0",(action,state),lowBound=0, cat='Integer')
    variables_real = LpVariable.dicts("Y",(horizon,action,state),lowBound=0.,cat='Continuous')
    # resource constraints 
    for j in resource:
        prob += lpSum([variables_int[a][s]*D[j,a,s] for s in state for a in action]) <= b[j]*N
    for t in horizon:
        for j in resource:
            prob += lpSum([variables_real[t][a][s]*D[j,a,s] for s in state for a in action]) <= b[j]*N
    # Markov state evolution
    if T > 1:
        for s in state:
            prob += lpSum(variables_real[1][a][s] for a in action) == lpSum([variables_int[a][ss]*P[a][ss][s] 
                                                                             for a in action for ss in state])
        for t in range(1,T-1):
            for s in state:
                prob += lpSum(variables_real[t+1][a][s] for a in action) == lpSum([variables_real[t][a][ss]*P[a][ss][s] 
                                                                                   for a in action for ss in state])
    # initial condition        
    for s in state:
        prob += lpSum(variables_int[a][s] for a in action) == states[s]
    # objective    
    prob += lpSum([variables_int[a][s]*R[a][s] for a in action for s in state] + 
                  [variables_real[t][a][s]*R[a][s] for t in horizon for a in action for s in state])
    
    prob.solve()
    
    return value(prob.objective)/N


