\documentclass{article}

\usepackage{amsthm,amsmath,amsfonts}
\DeclareMathAlphabet{\mathpzc}{OT1}{pzc}{m}{it}

\newcommand\bm{\mathbf{m}}
\newcommand\bs{\mathbf{s}}
\newcommand\ba{\mathbf{a}}
\newcommand\ms{\mathpzc{m}}
\newcommand\norm[1]{\left\Vert #1\right\Vert_1}
\newcommand\Proba[1]{\mathbf{P}\left(#1\right)}
\newcommand\Vrel{V^{rel}}
\newtheorem{lemma}{Lemma}
\newcommand\esp[1]{\mathbf{E}\left[#1\right]}
\newcommand\calS{\mathcal{S}}
\newcommand\calA{\mathcal{A}}
\newcommand{\DeltaN}{\Delta^{(N)}}

\title{Lipschitz properties of $\Vrel$}
\author{Nicolas Gast}

\begin{document}

\maketitle 

In this document, we study the Lipschitz-property of the function $\Vrel$ studied in \cite{gast2022lp}. Notations are identical to those of the aforementioned paper.

\section{Lipschitz-constant of $\Vrel$}

We define $\gamma$ as:
\begin{align*}
    \gamma = \min_{i, j, a} \sum_k \min( P^0_{ik}, P^a_{jk})
\end{align*}
The value of $\gamma$ is very similar\footnote{The ergodic coefficient contains an additional minimization on actions and is equal to $\min_{i,j, a, b} \sum_k \min( P^{b}_{ik}, P^a_{jk})$. It is smaller or equal to $\gamma$.} to what is call the ergodic coefficient of a MDP (see \cite{Puterman:1994:MDP:528623}).

\begin{lemma}
    \label{lem:Lipschitz-lemma}
    Assume that $0\le R^a_s\le1$. Then, for all $\bm,\tilde{\bm}\in\Delta$, the value function satisfies: 
    \begin{align}
        \label{eq:lemma-Lipschitz1}
        |\Vrel_t(\bm)-\Vrel_t(\tilde{\bm})|\le \frac12 (1+(1-\gamma) + \dots (1-\gamma)^{t-1})\norm{\bm-\tilde{\bm}}.
    \end{align}
\end{lemma}
In particular, this shows the Lipschitz constant of $\Vrel_t$ is upper bounded by $t/2$. When $\gamma>0$, the Lipschitz constant of $\Vrel_t$ is upper bounded by $1/2\gamma$, regardless of $t$.

% \begin{align}
%     \label{eq:lemma-Lipschitz2}
%     |\Vrel_t(\bm)-\Vrel_t(\tilde{\bm})|\le \frac12\min(t, \frac1\gamma) \norm{\bm-\tilde{\bm}}.
% \end{align}

\begin{proof}
    As we will in the proof, one key idea is to work vectors of $N$ components $\bs\in\calS$ instead of the population state $\bm$. The use of vectors of components will allow use a sort of coupling between trajectories. To simplify our notation, we will need a way to go from vectors of $N$ components to population vectors. For a population vector $\bs\in\calS^N$, we denote by $\ms(\bs)$ the vector such that for $i\in\calS$: 
    \begin{align*}
        \ms_i(\bs) = \frac1N \sum_{n=1}^N \mathbf{1}_{\{s_n=i\}}.
    \end{align*}
    Recall that $\Delta$ is the simplex of dimension $d=|\calS|$ and that $\DeltaN$ is the set $\bm\in\Delta$, such that $Nm_s$ is an integer for all $s\in\{1\dots d\}$. For all $\bs\in\calS^N$, $\ms(\bs)\in\DeltaN$. Respectively, for all $\bm\in\DeltaN$, there exists $\bs\in\calS^N$ such that $\ms(\bs)=\bm$ (unique up to a permutation of its components).

    Our proof of the result uses the following lemma, that shows that the property holds for some particular values of $\bm$ and $\tilde{\bm}$:
    \begin{lemma}
        \label{lem:intermediate_Lipschitz}
        The results of Lemma~\ref{lem:Lipschitz-lemma} hold for all $\bm,\tilde{\bm}$ for which there exists a $N>0$ such that $\bm,\tilde{\bm}\in\DeltaN$ and $\norm{\bm-\tilde{\bm}}\le 2/N$.
    \end{lemma}

    \begin{proof}[Proof of Lemma~\ref{lem:intermediate_Lipschitz}]
        We prove the result by induction on $t$. For $t=0$, the result is clearly true as $V_t(\bm)=0$ for all $\bm\in\Delta$. 
        
        We now assume that it holds for $t-1\ge0$ and look at what happens for $t$. Let $\bm,\tilde{\bm}\in\DeltaN$ be two population vectors such that $\norm{\bm-\tilde{\bm}}\le2/N$ and $\bm\ne\tilde{\bm}$. This implies that there exists two states $i\ne j\in\{1\dots d\}$ such that $m_i=\tilde{m}_i+1/N$ and $m_j=\tilde{m}_j-1/N$ (all others being equal). This shows that there exist two vectors $\bs,\tilde{\bs}\in\calS^N$ such that $s_1=i$, $\tilde{s_1}=j$, $s_n=\tilde{s_n}$ for $n\ge2$, and $\ms(\bs)=m$, $\ms(\tilde{\bs})=\tilde{\bm}$.
        
        %This is true because for such $m$, there exist two states $i,j$ such that $m_i=\tilde{m}_i+1$ and $m_j=\tilde{m}_j-1$. We can choose $X_1=i$ and $X'_1=j$. 
        For a vector of states $\bs\in\calS^N$ and vector of actions $\ba=a_1\dots a_N\in\calA^N$, we denote by $Q(\bs,\ba)$ the value that we obtain by applying action $\ba$ for the first time step and then using the optimal policy (for the relaxed problem) afterwards.
        \begin{align}
            \label{eq:bellman-equation}
            Q(\bs,\ba) = \frac1N \sum_{n=1}^N R^{a_n}_{s_n} + \sum_{\bs'\in\calS^N} \Vrel_{t-1}(\ms(\bs')) \prod_{n=1}^{N}P^{a_n}_{s_n,s_n'}.
        \end{align}
        Let $\ba^*$ be the optimal action vector used for state $\bs$, that is $\Vrel(\bm)=Q(\bs,\ba^*)$. Let $\ba'$ be the action vector such that $\ba'_1=0$ and $\ba'_n=\ba^*_n$ for all $n\in\{2\dots N\}$. The action vector $\ba'$ is an admissible action vector for $\tilde{\bs}$ but not necessarily the optimal one and hence $\Vrel(\tilde{\bm})\ge Q(\tilde{\bs},\ba')$. Hence, we have:
        \begin{align}
            \Vrel_t(\bm) &- \Vrel_t(\tilde{\bm}) \le Q(\bs, \ba^*) - Q(\tilde{\bs},\ba')\nonumber\\
            &= \frac1N (R^{a^1}_{s_1}-R^{0}_{s'_1}) + \sum_{\bs'\in\calS^N} \Vrel_{t-1}(\ms(\bs')) (\prod_{n=1}^{N}P^{a^*_n}_{s_n,s_n'} - \prod_{n=1}^{N}P^{a'_n}_{\tilde{s}_n,s_n'})\nonumber\\
            &= \frac1N (R^{a^1}_{s_1}-R^{0}_{s'_1}) + \sum_{\bs'\in\calS^N} \prod_{n=2}^{N}P^{a^*_n}_{s_n,s_n'}\Vrel_{t-1}(\ms(\bs')) (P^{a_1}_{i,s_1'}-P^0_{j,s'_1}),\nonumber\\
            &= \frac1N (R^{a^1}_{s_1}-R^{0}_{s'_1}) + \sum_{s'_2\dots s_N\in\calS^{N-1}} \prod_{n=2}^{N}P^{a^*_n}_{s_n,s_n'}\sum_{k\in\calS}\Vrel_{t-1}(\ms(k,s'_2\dots s'_N)) (P^{a_1}_{i,k}-P^0_{j,k}),
            \label{eq:bounding_Lipschitz}
        \end{align}
        where we used Equation~\eqref{eq:bellman-equation} for the first equality and the fact that $\bs$ and $\tilde{\bs}$, and $\ba$ and $\ba'$ are equal for all components except the first one. The last equality is just a rewriting of the above equation where the special role of the first component is emphasized.

        % We denote by $(Z_n)_{n=2\dots N}$ a tuple of $N$ independent random variable such that $\Proba{Z_n=k}=P^{a^*_n}_{s_n,k}$. Equation~\eqref{eq:bounding_Lipschitz} rewrites as
        % \begin{align*}
        %     \Vrel_t(\bm) - \Vrel_t(\tilde{\bm}) \le \frac1N  (R^{a^*_1}_i - R^{0}_j)+ \esp{\sum_k \Vrel_{t-1}(\ms(k, Z_2\dots Z_N))(P^a_{ik}-P^0_{jk})}.
        % \end{align*}
        By using the assumption that $0\le R^{a}_{i,j}\le 1$, the first term of Equation~\eqref{eq:bounding_Lipschitz} is smaller than $1/N$. For the second term, let $\gamma_k = \min( P^0_{ik}, P^a_{jk})$. For any values of $z_2\dots z_n$, we have 
        \begin{align*}
            &\sum_k \Vrel_{t-1}(\ms(k, s'_2\dots s'_N))(P^a_{ik}-P^0_{jk})\\
            &= \sum_k \Vrel_{t-1}(\ms(k, s'_2\dots s'_N))(P^a_{ik}-\gamma_k)
            -\sum_k \Vrel_{t-1}(\ms(k, s'_2\dots s'_N))(P^0_{jk}-\gamma_k)\\
            &\le \max_k \Vrel_{t-1}(\ms(k, s'_2\dots s'_N))(1-\sum_k\gamma_k)
            - \min_{k'} \Vrel_{t-1}(\ms(k', s'_2\dots s'_N))(1-\sum_k\gamma_k)
        \end{align*}
        By construction, $\ms(k, s'_2\dots s'_N)$ and $\ms(k', s'_2\dots s'_N)$ are at distance $2/N$ when $k\ne k'$ (and $0$ otherwise). By using that $\gamma=\sum_k\gamma_k$, we can apply our induction hypothesis to Equation~\eqref{eq:bounding_Lipschitz} to show that
        \begin{align*}
            \Vrel_t(\bm) &- \Vrel_t(\tilde{\bm}) \le \frac1N( 1+ (1-\gamma)(1+(1-\gamma)+\dots(1-\gamma)^{t-2}))
        \end{align*}
        By exchanging the values of $\bm$ and $\bm'$ ans using that $2\norm{\bm-\tilde{\bm}}/N=1/N$, we obtain that:
        \begin{align*}
            |\Vrel_t(\bm) - \Vrel_t(\tilde{\bm})| &\le \frac1N( 1+ (1-\gamma)(1+(1-\gamma)+\dots(1-\gamma)^{t-2}))\\
            &= \frac12( 1+ (1-\gamma) + (1-\gamma)^{t-1})\norm{\bm-\tilde{\bm}}.
        \end{align*}
    \end{proof}
    Lemma~\ref{lem:intermediate_Lipschitz} implies that the result hold for all $\bm,\tilde{\bm}\in\DeltaN$ for all $N$ (for a given $N$, this can be shown by induction on $N\norm{\bm-\tilde{\bm}}/2$).  By continuity of the value function and since $\cup_{N>0}\DeltaN$ is dense in $\Delta$, this implies that the result holds for all $\bm,\tilde{\bm}\in\Delta$.
\end{proof}

The proof of Lemma~\ref{lem:Lipschitz-lemma} uses two ideas:
\begin{itemize}
    \item We represent the state as the vector $\bs$ instead of the population vector $\bm$. This allows to write a sort of coupling equation in Equation~\eqref{eq:bounding_Lipschitz}.
    \item We write $\Vrel_t$ as a function of $\Vrel_{t-1}$ in Equation~\eqref{eq:bellman-equation}.
\end{itemize}
The second step of the proof can be generalized to obtain an equation that links $\Vrel_t$ and $\Vrel_{t-\tau}$ for some $\tau\ge1$. By using more complex notations, one can then obtain a stronger result that uses the contraction coefficient for any $\tau$. More precisely, for a given $\tau\ge1$, we define $\lambda_\tau$ as: 
\begin{align*}
    \lambda_\tau = \min_{i, j, \mathbf{a}} \sum_k \min( (P^0\dots P^0)_{ik}, (P^{a_1}\dots P^{a_\tau})_{jk}),
\end{align*}
where $\mathbf{a}$ is a vector of $\tau$ consecutive actions. Note that $\lambda_1=\gamma$. Applying mutatis-mutandis the proof of Lemma~\ref{lem:Lipschitz-lemma} to an induction with steps of size $\tau$ gives the following result.
\begin{lemma}
    \label{lem:Lipschitz-lemma-ergo}
    Assume that $0\le R^a_s\le1$. Then, for all $\bm,\tilde{\bm}\in\Delta$ and all $\tau$ such that $\lambda_\tau>0$, the value function satisfies: 
    \begin{align}
        \label{eq:lemma-Lipschitz-ergo}
        |\Vrel_t(\bm)-\Vrel_t(\tilde{\bm})|\le \frac{\tau}{2\lambda_\tau} \norm{\bm-\tilde{\bm}}.
    \end{align}
\end{lemma}

\bibliographystyle{plain}
\bibliography{reference}

\end{document} 